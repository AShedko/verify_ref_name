# Philosophy Referat Name parser

If successful, prints description and errors,
otherwise shows where the error is.

To run, use 
```
sbt run
```
To test:
```
sbt test
```