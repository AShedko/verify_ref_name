package main.scala.RefName

import scala.io.StdIn.readLine
import fastparse.SingleLineWhitespace._
import fastparse._

import java.time.Year

sealed trait Subj

object Subj extends Enumeration {
  type Subj = Value
  val Phil, Hist = Value

  def parseSubj(s: String): Subj.Subj = if (s == "phil") Phil else Hist
}

import Subj._

case class Group(letter: String, year: Int, num: Int) {}

case class Referat(year: Year, asp_type: String, subj: Subj.Subj, spec: String, dep: String, group: Group,
                   teacher: String, student: String, theme: String) {
}

object NameParser {

  def string[_: P]: P[String] = P((CharIn("a-z") | CharIn("A-Z")).rep(1).!)

  def subj[_: P]: P[Subj.Subj] = P(("phil" | "hist").!).map(parseSubj)

  def spec[_: P]: P[String] = P(CharIn("0-9").rep(min = 6, "", 6).!)

  def year[_: P]: P[Year] = P(CharIn("0-9").rep(4).!).map(Year.parse)

  def dep[_: P]: P[String] = P(CharIn("0-9").rep(2).!)

  def num[_: P]: P[Int] = P(CharIn("0-9").rep(2).!).map(s => s.toInt)

  def group[_: P]: P[Group] = P(string ~ num ~ "_" ~ num).map(Group.tupled)

  def Name[_: P]: P[Referat] = P(year ~ "_" ~ string ~ "_" ~ subj ~ "_" ~ spec ~ "_"
    ~ dep ~ "_" ~ group ~ "_" ~ string ~ "_" ~ string ~ "_" ~ string ~ (".doc" | ".docx") ~ End).map(x => Referat.tupled(x))

  def Parse(inp: String):Parsed[Referat] =
    parse(inp, Name(_))
}

// 2021_asp_phil_150413_42_A20_104_KhoteevaMS_IvanovSV_karlPopper.doc
object main extends App {
  val ref_name = readLine()
  NameParser.Parse(ref_name) match {
    case Parsed.Success(v, _) =>
      println("OK")
      println("Year:",v.year)
      println("Type:",v.asp_type)
      println("Subject:",v.subj)
      println("Spec:",v.spec)
      println("Dep:",v.dep)
      println("Group:",v.group)
      println("Teacher Name:", v.teacher)
      println("Student Name:", v.student)
      println("Theme:", v.theme)
      if (v.year!=java.time.Year.now()) println("Wrong year")
    case failure: Parsed.Failure =>
      val t = failure.trace()
      println("Incorrect Referat name")
      println(t.failure)
  }
}