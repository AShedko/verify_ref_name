name := "ref_name_parse"

version := "0.1"

scalaVersion := "2.13.5"

Compile / scalaSource := baseDirectory.value / "src"
Test / scalaSource := baseDirectory.value / "test"

libraryDependencies ++= Seq(
  "com.lihaoyi" %% "fastparse" % "2.2.2",
  "org.scalatest" %% "scalatest" % "3.2.2" % Test,
  "com.github.scopt" %% "scopt" % "4.0.0-RC2")
