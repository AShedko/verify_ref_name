package scala.RefNameTest

import fastparse.Parsed
import org.scalatest.funsuite._
import main.scala.RefName._

class RefNameTest extends AnyFunSuite {
  def parse(x:String): Parsed[Referat] =NameParser.Parse(x)

  test("Parses Correct") {
    val x = parse("2020_asp_phil_150413_42_A20_104_KhoteevaMS_IvanovSV_karlPopper.doc")
    assert(x.isSuccess)
  }

  test("Fails on InCorrect") {
    val x = parse("2020_psls_history_150413_42_A20_104_KhoteevaMS_IvanovSV_karlPopper.doc")
    assert(!x.isSuccess)
  }
}